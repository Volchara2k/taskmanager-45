package ru.renessans.jvschool.volkov.task.manager.api.service.adapter;

import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Project;

public interface IProjectAdapterService extends IAdapterService<ProjectDTO, Project> {
}