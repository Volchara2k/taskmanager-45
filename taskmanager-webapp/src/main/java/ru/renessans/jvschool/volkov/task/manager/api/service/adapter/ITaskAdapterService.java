package ru.renessans.jvschool.volkov.task.manager.api.service.adapter;

import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Task;

public interface ITaskAdapterService extends IAdapterService<TaskDTO, Task> {
}