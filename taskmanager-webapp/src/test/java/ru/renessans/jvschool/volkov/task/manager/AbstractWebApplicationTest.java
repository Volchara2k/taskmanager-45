package ru.renessans.jvschool.volkov.task.manager;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.configuration.WebApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Task;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserProjectRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserTaskRepository;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public abstract class AbstractWebApplicationTest {

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    protected IUserService userService;

    @Autowired
    protected IUserTaskRepository userTaskRepository;

    @Autowired
    protected IUserProjectRepository userProjectRepository;

    @Autowired
    protected AuthenticationManager authenticationManager;

    @NotNull
    protected static final Project PROJECT = new Project();

    @NotNull
    protected static final Task TASK = new Task();

    protected static User USER;

    protected MockMvc mockMvc;

    @BeforeClass
    public static void initialDataBeforeClass() {
        @NotNull final String project1st = "project1st";
        PROJECT.setTitle(project1st);
        PROJECT.setDescription(project1st);
        @NotNull final String task1st = "task1st";
        TASK.setTitle(task1st);
        TASK.setDescription(task1st);
        TASK.setProject(PROJECT);
    }

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .build();
        USER = this.userService.getUserByLogin("test");
        PROJECT.setUser(USER);
        TASK.setUser(USER);
        this.userProjectRepository.save(PROJECT);
        this.userTaskRepository.save(TASK);

        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                "test", "test"
        );
        @NotNull final Authentication authentication = this.authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

}