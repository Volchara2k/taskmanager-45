package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;

public interface IDataInterChangeService {

    boolean dataBinClear();

    boolean dataBase64Clear();

    boolean dataJsonClear();

    boolean dataXmlClear();

    boolean dataYamlClear();

    @NotNull
    DomainDTO exportDataBin();

    @NotNull
    DomainDTO exportDataBase64();

    @NotNull
    DomainDTO exportDataJson();

    @NotNull
    DomainDTO exportDataXml();

    @NotNull
    DomainDTO exportDataYaml();

    @NotNull
    DomainDTO importDataBin();

    @NotNull
    DomainDTO importDataBase64();

    @NotNull
    DomainDTO importDataJson();

    @NotNull
    DomainDTO importDataXml();

    @NotNull
    DomainDTO importDataYaml();

}