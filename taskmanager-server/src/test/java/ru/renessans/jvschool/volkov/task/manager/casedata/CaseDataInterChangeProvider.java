package ru.renessans.jvschool.volkov.task.manager.casedata;

@SuppressWarnings("unused")
public final class CaseDataInterChangeProvider {

    public Object[] invalidFilenamesCaseData() {
        return new Object[]{
                new Object[]{
                        "test",
                        null
                },
                new Object[]{
                        "string",
                        ""
                },
                new Object[]{
                        "demo",
                        "   "
                }
        };
    }

    public Object[] invalidFilenamesClassCaseData() {
        return new Object[]{
                new Object[]{String.class, null},
                new Object[]{String.class, ""},
                new Object[]{String.class, "    "}
        };
    }

    public Object[] validClassesCaseData() {
        return new Object[]{
                new Object[]{String.class, "fg6A0Induv"},
                new Object[]{String.class, "JyWYAYmqJf"},
                new Object[]{String.class, "nnerVw24PV"}
        };
    }

    public Object[] validLinesCaseData() {
        return new Object[]{
                new Object[]{"demo", "fg6A0Induv"},
                new Object[]{".....", "JyWYAYmqJf"},
                new Object[]{"!23", "nnerVw24PV"}
        };
    }

}