package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

@Setter
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class UserProjectServiceTest {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IUserProjectService userProjectService;

    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.userProjectService);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testUpdateByIndex() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Project addProject = this.userProjectService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addProject);

        @NotNull final String newData = UUID.randomUUID().toString();
        Assert.assertNotNull(newData);
        @Nullable final Project updateProject = this.userProjectService.updateUserOwnerByIndex(user.getId(), 0, newData, newData);
        Assert.assertNotNull(updateProject);
        Assert.assertEquals(newData, updateProject.getTitle());
        Assert.assertEquals(newData, updateProject.getDescription());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testUpdateById() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Project addProject = this.userProjectService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addProject);

        @NotNull final String newData = UUID.randomUUID().toString();
        Assert.assertNotNull(newData);
        @Nullable final Project updateProject = this.userProjectService.updateUserOwnerById(user.getId(), addProject.getId(), newData, newData);
        Assert.assertNotNull(updateProject);
        Assert.assertEquals(addProject.getId(), updateProject.getId());
        Assert.assertEquals(addProject.getUserId(), updateProject.getUserId());
        Assert.assertEquals(newData, updateProject.getTitle());
        Assert.assertEquals(newData, updateProject.getDescription());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDeleteByIndex() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Project addProject = this.userProjectService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addProject);

        final int deleteProject = this.userProjectService.deleteUserOwnerByIndex(user.getId(), 0);
        Assert.assertEquals(1, deleteProject);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDeleteById() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Project addProject = this.userProjectService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addProject);

        final int deleteProject = this.userProjectService.deleteUserOwnerById(user.getId(), addProject.getId());
        Assert.assertEquals(1, deleteProject);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDeleteByTitle() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Project addProject = this.userProjectService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addProject);

        final int deleteProject = this.userProjectService.deleteUserOwnerByTitle(user.getId(), addProject.getTitle());
        Assert.assertEquals(1, deleteProject);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDeleteAll() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Project addProject = this.userProjectService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addProject);

        final int deleteProjects = this.userProjectService.deleteUserOwnerAll(user.getId());
        Assert.assertNotEquals(0, deleteProjects);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetByIndex() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Project addProject = this.userProjectService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addProject);

        @Nullable final Project project = this.userProjectService.getUserOwnerByIndex(user.getId(), 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(addProject.getId(), project.getId());
        Assert.assertEquals(addProject.getUserId(), project.getUserId());
        Assert.assertEquals(addProject.getTitle(), project.getTitle());
        Assert.assertEquals(addProject.getDescription(), project.getDescription());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetById() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Project addProject = this.userProjectService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addProject);

        @Nullable final Project project = this.userProjectService.getUserOwnerByTitle(user.getId(), addProject.getTitle());
        Assert.assertNotNull(project);
        Assert.assertEquals(addProject.getId(), project.getId());
        Assert.assertEquals(addProject.getUserId(), project.getUserId());
        Assert.assertEquals(addProject.getTitle(), project.getTitle());
        Assert.assertEquals(addProject.getDescription(), project.getDescription());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetByTitle() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Project addProject = this.userProjectService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addProject);

        @Nullable final Project project = this.userProjectService.getUserOwnerByTitle(user.getId(), addProject.getTitle());
        Assert.assertNotNull(project);
        Assert.assertEquals(addProject.getId(), project.getId());
        Assert.assertEquals(addProject.getUserId(), project.getUserId());
        Assert.assertEquals(addProject.getTitle(), project.getTitle());
        Assert.assertEquals(addProject.getDescription(), project.getDescription());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetAll() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Project addProject = this.userProjectService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addProject);

        @Nullable final Collection<Project> projects = this.userProjectService.getUserOwnerAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertNotEquals(0, projects.size());
        final boolean isUserProjects = projects.stream().allMatch(entity -> user.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserProjects);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testInitialDemoData() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Collection<User> users = Collections.singletonList(user);
        Assert.assertNotNull(users);

        @NotNull final Collection<Project> initProjects = this.userProjectService.initialUserOwner(users);
        Assert.assertNotNull(initProjects);
    }


//    @Test
//    @TestCaseName("Run testNegativeAdd for add(\"{0}\", \"{1}\", \"{2}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidOwnerMainFieldsCaseData"
//    )
//    public void testNegativeAdd(
//            @Nullable final String userId,
//            @Nullable final String title,
//            @Nullable final String description
//    ) {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userProjectService.addUserOwner(userId, title, description)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> userProjectService.addUserOwner(userIdTemp, title, description)
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//
//        @NotNull final String titleTemp = UUID.randomUUID().toString();
//        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
//                InvalidDescriptionException.class,
//                () -> userProjectService.addUserOwner(userIdTemp, titleTemp, description)
//        );
//        Assert.assertNotNull(descriptionThrown);
//        Assert.assertNotNull(descriptionThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeUpdateByIndex for updateByIndex(\"{0}\", {1}, \"{2}\", \"{3}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidOwnerWithIndexCaseData"
//    )
//    public void testNegativeUpdateByIndex(
//            @Nullable final String userId,
//            @Nullable final Integer index,
//            @Nullable final String title,
//            @Nullable final String description
//    ) {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userProjectService.updateUserOwnerByIndex(userId, index, title, description)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final IllegalIndexException indexThrown = assertThrows(
//                IllegalIndexException.class,
//                () -> userProjectService.updateUserOwnerByIndex(userIdTemp, index, title, description)
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//
//        @NotNull final Integer indexTemp = 0;
//        Assert.assertNotNull(indexTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> userProjectService.updateUserOwnerByIndex(userIdTemp, indexTemp, title, description)
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//
//        @NotNull final String titleTemp = UUID.randomUUID().toString();
//        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
//                InvalidDescriptionException.class,
//                () -> userProjectService.addUserOwner(userIdTemp, titleTemp, description)
//        );
//        Assert.assertNotNull(descriptionThrown);
//        Assert.assertNotNull(descriptionThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeUpdateById for updateById(\"{0}\", \"{1}\", \"{2}\", \"{3}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidOwnerWithIdCaseData"
//    )
//    public void testNegativeUpdateById(
//            @Nullable final String userId,
//            @Nullable final String id,
//            @Nullable final String title,
//            @Nullable final String description
//    ) {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userProjectService.updateUserOwnerById(userId, id, title, description)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidIdException indexThrown = assertThrows(
//                InvalidIdException.class,
//                () -> userProjectService.updateUserOwnerById(userIdTemp, id, title, description)
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//
//        @NotNull final String idTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(idTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> userProjectService.updateUserOwnerById(userIdTemp, idTemp, title, description)
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//
//        @NotNull final String titleTemp = UUID.randomUUID().toString();
//        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
//                InvalidDescriptionException.class,
//                () -> userProjectService.addUserOwner(userIdTemp, titleTemp, description)
//        );
//        Assert.assertNotNull(descriptionThrown);
//        Assert.assertNotNull(descriptionThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeDeleteByIndex for deleteByIndex(\"{0}\", {1})")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidOwnerIndexCaseData"
//    )
//    public void testNegativeDeleteByIndex(
//            @Nullable final String userId,
//            @Nullable final Integer index
//    ) {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userProjectService.deleteUserOwnerByIndex(userId, index)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final IllegalIndexException indexThrown = assertThrows(
//                IllegalIndexException.class,
//                () -> userProjectService.deleteUserOwnerByIndex(userIdTemp, index)
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeDeleteById for deleteById(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeDeleteById(
//            @Nullable final String userId,
//            @Nullable final String id
//    ) {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userProjectService.deleteUserOwnerById(userId, id)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidIdException idThrown = assertThrows(
//                InvalidIdException.class,
//                () -> userProjectService.deleteUserOwnerById(userIdTemp, id)
//        );
//        Assert.assertNotNull(idThrown);
//        Assert.assertNotNull(idThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeDeleteByTitle for deleteByTitle(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeDeleteByTitle(
//            @Nullable final String userId,
//            @Nullable final String id
//    ) {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userProjectService.deleteUserOwnerById(userId, id)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> userProjectService.deleteUserOwnerByTitle(userIdTemp, id)
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//    }
//
//    @Test(expected = InvalidUserIdException.class)
//    @TestCaseName("Run testNegativeDeleteAll for deleteAll(\"{0}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataBaseProvider.class,
//            method = "invalidLinesCaseData"
//    )
//    public void testNegativeDeleteAll(
//            @Nullable final String userId
//    ) {
//        userProjectService.deleteUserOwnerAll(userId);
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeGetByIndex for getByIndex(\"{0}\", {1})")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidOwnerIndexCaseData"
//    )
//    public void testNegativeGetByIndex(
//            @Nullable final String userId,
//            @Nullable final Integer index
//    ) {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userProjectService.getUserOwnerByIndex(userId, index)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final IllegalIndexException indexThrown = assertThrows(
//                IllegalIndexException.class,
//                () -> userProjectService.getUserOwnerByIndex(userIdTemp, index)
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeGetById for getById(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeGetById(
//            @Nullable final String userId,
//            @Nullable final String id
//    ) {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userProjectService.getUserOwnerById(userId, id)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidIdException idThrown = assertThrows(
//                InvalidIdException.class,
//                () -> userProjectService.getUserOwnerById(userIdTemp, id)
//        );
//        Assert.assertNotNull(idThrown);
//        Assert.assertNotNull(idThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeGetByTitle for getByTitle(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeGetByTitle(
//            @Nullable final String userId,
//            @Nullable final String title
//    ) {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userProjectService.getUserOwnerByTitle(userId, title)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> userProjectService.getUserOwnerByTitle(userIdTemp, title)
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//    }
//
//    @Test(expected = InvalidUserIdException.class)
//    @TestCaseName("Run testNegativeGetAll for getAll(\"{0}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataBaseProvider.class,
//            method = "invalidLinesCaseData"
//    )
//    public void testNegativeGetAll(
//            @Nullable final String userId
//    ) {
//        userProjectService.getUserOwnerAll(userId);
//    }
//
//    @Test(expected = InvalidUserException.class)
//    @TestCaseName("Run testInitialDemoData for initialDemoData({0})")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidCollectionsUsersCaseData"
//    )
//    public void testNegativeInitialDemoData(
//            @Nullable final Collection<User> users
//    ) {
//        userProjectService.initialUserOwner(users);
//    }
//
//    @Test
//    @TestCaseName("Run testAdd for add({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testAdd(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final Project addProject = userProjectService.addUserOwner(user.getId(), project.getTitle(), project.getDescription());
//        Assert.assertNotNull(addProject);
//        Assert.assertEquals(project.getUserId(), addProject.getUserId());
//        Assert.assertEquals(project.getTitle(), addProject.getTitle());
//        Assert.assertEquals(project.getDescription(), addProject.getDescription());
//        @Nullable final Project cleared = userProjectService.deleteRecord(addProject);
//        Assert.assertNotNull(cleared);
//    }

}