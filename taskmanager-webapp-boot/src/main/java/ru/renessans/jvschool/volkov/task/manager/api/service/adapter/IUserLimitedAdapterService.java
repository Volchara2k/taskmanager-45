package ru.renessans.jvschool.volkov.task.manager.api.service.adapter;

import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.entity.User;

public interface IUserLimitedAdapterService extends IAdapterService<UserLimitedDTO, User> {
}