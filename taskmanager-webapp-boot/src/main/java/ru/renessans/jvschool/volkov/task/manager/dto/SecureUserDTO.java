package ru.renessans.jvschool.volkov.task.manager.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@Getter
@Setter
public final class SecureUserDTO extends User {

    @NotNull
    private String id;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Builder(
            builderMethodName = "detailBuilder"
    )
    public SecureUserDTO(
            @NotNull final UserDetails userDetails,
            @NotNull final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) {
        super(
                userDetails.getUsername(), userDetails.getPassword(),
                userDetails.isEnabled(), userDetails.isAccountNonExpired(),
                userDetails.isCredentialsNonExpired(), userDetails.isAccountNonLocked(),
                userDetails.getAuthorities()
        );
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

}