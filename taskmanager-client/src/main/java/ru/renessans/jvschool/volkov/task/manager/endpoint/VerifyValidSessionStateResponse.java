
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for verifyValidSessionStateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="verifyValidSessionStateResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sessionValidState" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}sessionValidState" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "verifyValidSessionStateResponse", propOrder = {
    "sessionValidState"
})
public class VerifyValidSessionStateResponse {

    @XmlSchemaType(name = "string")
    protected SessionValidState sessionValidState;

    /**
     * Gets the value of the sessionValidState property.
     * 
     * @return
     *     possible object is
     *     {@link SessionValidState }
     *     
     */
    public SessionValidState getSessionValidState() {
        return sessionValidState;
    }

    /**
     * Sets the value of the sessionValidState property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionValidState }
     *     
     */
    public void setSessionValidState(SessionValidState value) {
        this.sessionValidState = value;
    }

}
